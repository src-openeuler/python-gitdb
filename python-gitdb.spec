%global _empty_manifest_terminate_build 0
Name:           python-gitdb
Version:        4.0.11
Release:        1
Summary:        Git Object Database
License:        BSD-3-Clause
URL:            https://github.com/gitpython-developers/gitdb
Source0:        https://files.pythonhosted.org/packages/19/0d/bbb5b5ee188dec84647a4664f3e11b06ade2bde568dbd489d9d64adef8ed/gitdb-4.0.11.tar.gz
BuildArch:      noarch
%description
GitDB allows you to access bare git repositories for reading and writing.
It aims at allowing full access to loose objects as well as packs with
performance and scalability in mind. It operates exclusively on streams,
allowing to handle large objects with a small memory footprint.

%package -n python3-gitdb
Summary:        Git Object Database
Provides:       python-gitdb = %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-smmap
# General requires
Requires:       python3-smmap
%description -n python3-gitdb
GitDB allows you to access bare git repositories for reading and writing.
It aims at allowing full access to loose objects as well as packs with
performance and scalability in mind. It operates exclusively on streams,
allowing to handle large objects with a small memory footprint.

%package help
Summary:        Git Object Database
Provides:       python3-gitdb-doc
%description help
GitDB allows you to access bare git repositories for reading and writing.
It aims at allowing full access to loose objects as well as packs with
performance and scalability in mind. It operates exclusively on streams,
allowing to handle large objects with a small memory footprint.

%prep
%autosetup -n gitdb-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-gitdb -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Dec 07 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 4.0.11-1
- Update package to version 4.0.11

* Fri Dec 16 2022 liqiuyu <liqiuyu@kylinos.cn> - 4.0.10-1
- Update package to version 4.0.10

* Tue May 31 2022 OpenStack_SIG <openstack@openeuler.org> - 4.0.9-1
- Upgrade package python3-gitdb to version 4.0.9

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Update to 4.0.5

* Mon Oct 26 2020 Jiachen Fan <fanjiachen3@huawei.com> - 4.0.1-1
- package init
